package org.pneditor.petrinet.adapters.DorinPalmade;

import org.pneditor.petrinet.AbstractTransition;
import org.pneditor.petrinet.models.DorinPalmade.*;

public class TransitionAdapter extends AbstractTransition {

	private Transition t;
	
	/**
     * Constructeur de la classe TransitionAdapter.
     *
     * @param label Le libellé de la transition.
     * @param t     La transition sous-jacente.
     */
	public TransitionAdapter(String label, Transition t) {
		super(label);
		this.t = t;
		// TODO Auto-generated constructor stub
	}

	/**
     * Obtient l'objet Transition sous-jacent.
     *
     * @return L'objet Transition.
     */
	public Transition getTransition() {
		return this.t;
	}
	
	/**
     * Indique si la transition est une place.
     *
     * @return False (car une transition n'est jamais une place)
     */
	@Override
	public boolean isPlace() {
		return false;
	}
}
