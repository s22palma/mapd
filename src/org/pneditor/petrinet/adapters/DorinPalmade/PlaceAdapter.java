package org.pneditor.petrinet.adapters.DorinPalmade;

import org.pneditor.petrinet.AbstractPlace;
import org.pneditor.petrinet.models.DorinPalmade.*;

public class PlaceAdapter extends AbstractPlace {
	private Place p;

	/**
     * Constructeur de la classe PlaceAdapter.
     *
     * @param label Le libellé de la place.
     * @param p     La place sous-jacente.
     */
	public PlaceAdapter(String label, Place p) {
		super("");
		this.p = p;
	}
	
	/**
     * Obtient l'objet Place sous-jacent.
     *
     * @return L'objet Place.
     */
	public Place getPlace() {
		return this.p;
	}

	/**
     * Ajoute un jeton à la place.
     */
	@Override
	public void addToken() {
		this.p.ajouterJetons(1);

	}

	/**
     * Supprime un jeton de la place.
     */
	@Override
	public void removeToken() {
		this.p.supprimerJetons(1);

	}

	/**
     * Obtient le nombre de jetons dans la place.
     *
     * @return Le nombre de jetons.
     */
	@Override
	public int getTokens() {
		return p.getJetons();
	}

	/**
     * Définit le nombre de jetons dans la place.
     *
     * @param tokens Le nouveau nombre de jetons.
     */
	@Override
	public void setTokens(int tokens) {
		this.p.setJetons(tokens);

	}

}
