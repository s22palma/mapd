package org.pneditor.petrinet.adapters.DorinPalmade;

import org.pneditor.petrinet.AbstractArc;

import org.pneditor.petrinet.AbstractNode;
import org.pneditor.petrinet.ResetArcMultiplicityException;

import org.pneditor.petrinet.models.DorinPalmade.*;


public class ArcAdapter extends AbstractArc {

	private Arc arc;
	private TransitionAdapter aTransition;
	
	private PlaceAdapter aPlace;
	private boolean entrant;
	private boolean sortant;
	private boolean zero;
	private boolean videur;
	

	/**
     * Construit un ArcAdapter.
     *
     * @param arc         L'objet Arc sous-jacent.
     * @param aTransition Le TransitionAdapter associé.
     */
	public ArcAdapter(Arc arc, TransitionAdapter aTransition) {
		super();
		this.arc = arc;
		this.aTransition = aTransition;
		
		this.aPlace = new PlaceAdapter(this.arc.getPlace().getId()+"", this.arc.getPlace());
		this.entrant = this.arc.estEntrant() ;
		this.sortant = this.arc.estSortant();
		this.zero = this.arc.estZero();
		this.videur = this.arc.estVideur();
	}
	
	/**
     * Obtient l'objet Arc sous-jacent.
     *
     * @return L'objet Arc.
     */
	public Arc getArc() {
		return this.arc;
	}

	/**
     * Obtient le nœud source de l'arc.
     *
     * @return Le nœud source, qui est soit un TransitionAdapter, soit un PlaceAdapter.
     */
	@Override
	public AbstractNode getSource() {
		if (this.entrant) {
			return this.aTransition;
		}
		else {
			return this.aPlace;
		}
	}

	/**
     * Obtient le nœud de destination de l'arc.
     *
     * @return Le nœud de destination, qui est soit un PlaceAdapter, soit un TransitionAdapter.
     */
	@Override
	public AbstractNode getDestination() {
		if (this.entrant) {
			return this.aPlace;
		}
		else {
			return this.aTransition;
		}
	}

	/**
     * Vérifie si l'arc est un arc de réinitialisation.
     *
     * @return Vrai si l'arc est un arc de réinitialisation, faux sinon.
     */
	@Override
	public boolean isReset() {
		return this.videur;
	}

	/**
     * Vérifie si l'arc est un arc régulier (ni zéro ni de réinitialisation).
     *
     * @return Vrai si l'arc est régulier, faux sinon.
     */
	@Override
	public boolean isRegular() {
		return !(this.zero && this.videur);
	}
	
	/**
     * Vérifie si l'arc est inhibiteur.
     *
     * @return Vrai si l'arc est inhibiteur, faux sinon.
     */
	@Override
	public boolean isInhibitory() {
		return this.zero;
	}

	/**
     * Obtient le poids de l'arc.
     *
     * @return Le poids de l'arc.
     * @throws ResetArcMultiplicityException Si le poids de l'arc est négatif.
     */
	@Override
	public int getMultiplicity() throws ResetArcMultiplicityException {
		if (this.arc.getPoids()<0) {
			throw new ResetArcMultiplicityException();
		}
		return this.arc.getPoids();
	}

	/**
     * Définit le poids de l'arc.
     *
     * @param multiplicity La nouvelle valeur du poids.
     * @throws ResetArcMultiplicityException Si une tentative est faite pour définir un poids négative
     */
	@Override
	public void setMultiplicity(int multiplicity) throws ResetArcMultiplicityException {
		if (multiplicity <0) {
			throw new ResetArcMultiplicityException();
		}
		this.arc.setPoids(multiplicity);
	}

}
