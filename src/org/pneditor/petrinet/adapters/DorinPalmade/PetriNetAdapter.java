package org.pneditor.petrinet.adapters.DorinPalmade;
import org.pneditor.petrinet.*;
import org.pneditor.petrinet.PetriNetInterface;
import org.pneditor.petrinet.models.DorinPalmade.*;


public class PetriNetAdapter extends PetriNetInterface{

	private ReseauPetri rp;
	
	/**
     * Constructeur par défaut qui initialise un nouveau réseau de Petri.
     */
	public PetriNetAdapter() {
		this.rp = new ReseauPetri();
	}
	
	/**
     * Constructeur prenant un réseau de Petri existant.
     *
     * @param rp Le réseau de Petri existant.
     */
	public PetriNetAdapter(ReseauPetri rp) {
		super();
		this.rp =rp;
	}
	
	/**
     * Ajoute une place au réseau de Petri.
     *
     * @return La place ajoutée.
     */
	@Override
	public AbstractPlace addPlace() {
		PlaceAdapter place = new PlaceAdapter("", new Place(0));
		this.rp.ajouterPlace(place.getPlace());
		return place;
	}

	/**
     * Ajoute une transition au réseau de Petri.
     *
     * @return La transition ajoutée.
     */
	@Override
	public AbstractTransition addTransition() {
		TransitionAdapter transition = new TransitionAdapter("", new Transition());
		this.rp.ajouterTransition(transition.getTransition());
		return transition;
	}

	/**
     * Ajoute un arc qui est ni zéro ni videur entre deux nœuds (une transition et une place).
     *
     * @param source      Le nœud source.
     * @param destination Le nœud de destination.
     * @return L'arc ajouté.
     * @throws UnimplementedCaseException Si le cas n'est pas implémenté.
     */
	@Override
	public AbstractArc addRegularArc(AbstractNode source, AbstractNode destination) throws UnimplementedCaseException {
		if (source.isPlace()) {
			Place placeSource = ((PlaceAdapter) source).getPlace();
			Sortant arc= new Sortant(0, placeSource);
			ArcAdapter aArc = new ArcAdapter((Sortant) arc, (TransitionAdapter) destination);
			((TransitionAdapter)destination).getTransition().ajouterSortant(arc);
			return aArc;
		}
		else {
			Place placeDestination = ((PlaceAdapter) destination).getPlace();
			Entrant arc= new Entrant(0, placeDestination);
			ArcAdapter aArc = new ArcAdapter((Entrant) arc, (TransitionAdapter) source );
			((TransitionAdapter)source).getTransition().ajouterEntrant(arc);
			return aArc;
		}
	}

	/**
     * Ajoute un arc zero entre une place et une transition.
     *
     * @param place      La place source.
     * @param transition La transition (destination de l'arc).
     * @return L'arc ajouté.
     * @throws UnimplementedCaseException Si le cas n'est pas implémenté.
     */
	@Override
	public AbstractArc addInhibitoryArc(AbstractPlace place, AbstractTransition transition)
			throws UnimplementedCaseException {
		Place placeSource = ((PlaceAdapter) place).getPlace();
		Zero arc= new Zero(0, placeSource);
		ArcAdapter aArc = new ArcAdapter((Zero) arc, (TransitionAdapter) transition);
		((TransitionAdapter)transition).getTransition().ajouterSortant(arc);
		return aArc;
	}
	
	
	/**
     * Ajoute un arc videur entre une place et une transition.
     *
     * @param place      La place source.
     * @param transition La transition (destination de l'arc).
     * @return L'arc ajouté.
     * @throws UnimplementedCaseException Si le cas n'est pas implémenté.
     */
	@Override
	public AbstractArc addResetArc(AbstractPlace place, AbstractTransition transition)
			throws UnimplementedCaseException {
		Place placeSource = ((PlaceAdapter) place).getPlace();
		Videur arc= new Videur(0, placeSource);
		ArcAdapter aArc = new ArcAdapter((Videur) arc, (TransitionAdapter) transition);
		((TransitionAdapter)transition).getTransition().ajouterSortant(arc);
		return aArc;
	}

	/**
     * Supprime une place du réseau de Petri.
     *
     * @param place La place à supprimer.
     */
	@Override
	public void removePlace(AbstractPlace place) {
		this.rp.supprimerPlace(place.getId());
		
	}

	/**
     * Supprime une transition du réseau de Petri.
     *
     * @param transition La transition à supprimer.
     */
	@Override
	public void removeTransition(AbstractTransition transition) {
		this.rp.supprimerTransition(transition.getId());
	}

	/**
     * Supprime un arc du réseau de Petri.
     *
     * @param arc L'arc à supprimer.
     */
	@Override
	public void removeArc(AbstractArc arc) {
		int idArc = ((ArcAdapter) arc).getArc().getId();
		this.rp.supprimerArc(idArc);	
	}

	/**
     * Regarde si la transition peut être tirée.
     *
     * @param transition
     * @return true si la transition est tirable, false sinon
     */
	@Override
	public boolean isEnabled(AbstractTransition transition) throws ResetArcMultiplicityException {
		boolean sortie = true;
		for (Sortant s: ((TransitionAdapter) transition).getTransition().getSortant() ) {
			sortie = sortie && s.estTirable();
		}
		return sortie;
	}

	/**
     * Tire la transition.
     *
     * @param transition La transition à tirer
     */
	@Override
	public void fire(AbstractTransition transition) throws ResetArcMultiplicityException {
		try { ((TransitionAdapter) transition).getTransition().incremente();}
		catch (NonTirable e) {
			throw new ResetArcMultiplicityException();
		}
	}

}
