/**
 * Ce package fait la liaison entre l'implémentation de notre réseau de Pétri 
 (package org.pneditor.petrinet.models.DorinPalmade) et le PNE, pour pouvoir 
 utiliser l'interface graphique qui y est codé.
 */
 
package org.pneditor.petrinet.adapters.DorinPalmade;