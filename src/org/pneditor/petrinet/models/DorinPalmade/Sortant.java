package org.pneditor.petrinet.models.DorinPalmade;

/**
 * La classe 'Sortant' qui hérite de la classe 'Arc' représente un arc dans le réseau de Pétri qui connecte une place à une transition.
 * (Il sort de la place)
 * L'attribut place de cette classe réfère donc à la classe en amont de l'arc sortant.
 */

public class Sortant extends Arc {
	
	//CONSTRUCTEUR
	public Sortant(int poids, Place place) {
		super(poids, place);
	}

	//METHODES
	
	/**
	 * Vérifie que l'arc peut être tiré, c'est à dire que la place en attribut a suffisamment de jetons (au moins le poids de l'arc)
	 * 
	 * @return 'true' s'il peut être tiré, 'false' sinon.
	 */
	public boolean estTirable() {
		return this.getPlace().comparerJetons(this.getPoids());
	}
	
	/**
	 * Retire un nombre de jetons à la place en attribut égal à son poids
	 */
	public void tirer() {
		this.getPlace().supprimerJetons(this.getPoids());
	}
	
	@Override
	public boolean estSortant() {
		return true;
	}
	
	
	
}
