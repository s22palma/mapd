package org.pneditor.petrinet.models.DorinPalmade;

public class NonTirable extends Exception {

	/**
	 * Constructeur de la classe NonTirable.
	 * Affiche un message indiquant qu'une transition n'a pas pu être tirée.
	 */
	public NonTirable() {
		System.out.println("La transition n'a pas pu être tiré");
	}

}
