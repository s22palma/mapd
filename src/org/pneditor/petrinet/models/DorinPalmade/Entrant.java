package org.pneditor.petrinet.models.DorinPalmade;

/**
 * La classe 'Entrant' qui hérite de la classe 'Arc' représente un arc dans le réseau de Pétri qui connecte une transition à une place.
 * (Il entre dans la place)
 * L'attribut place de cette classe réfère donc à la classe en aval de l'arc entrant.
 */

public class Entrant extends Arc {
	
	//CONSTRUCTEURS
	public Entrant(int poids, Place place) {
		super(poids, place);
	}

	//METHODES
	
	/**
	 * Ajoute à la place en attribut un nombre de jetons égal au poids de l'arc
	 */ 
	public void tirer() {
		this.getPlace().ajouterJetons(this.getPoids());;
	}
	
	@Override
	public boolean estEntrant() {
		return true;
	}
		
}
