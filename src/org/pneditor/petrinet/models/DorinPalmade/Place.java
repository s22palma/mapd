package org.pneditor.petrinet.models.DorinPalmade;

public class Place {
	
	private static int NB_PLACE=0;
	private final int id; //id identifiant la place
	private int jetons; //Nombre de jetons dans la place

	// Constructeur
	
	public Place(int jetons) {
		NB_PLACE++;
		this.id=NB_PLACE;
		this.jetons = jetons;
	}
	
	// Getters et Setters
	
	public int getJetons() {
		return jetons;
	}

	
	public void setJetons(int jetons) {
		if(jetons>=0) this.jetons = jetons;
	}
	
	public int getId() {
		return id;
	}
	
	//Méthodes
	
	/**
	 * Cette méthode supprime un nombre spécifié de jetons de la place, à condition que le nombre soit non négatif.
	 *
	 * @param nombre Le nombre de jetons à supprimer.
	 */
	public void supprimerJetons(int nombre) {
		if(nombre>=0) this.setJetons(this.getJetons()-nombre);
	}
	
	/**
	 * Cette méthode ajoute un nombre spécifié de jetons à la place, à condition que le nombre soit non négatif.
	 *
	 * @param nombre Le nombre de jetons à ajouter.
	 */
	public void ajouterJetons(int nombre) {
		if(nombre>=0) this.setJetons(this.getJetons()+nombre);
	}
	
	/**
	 * Cette méthode compare le nombre de jetons dans la place avec un nombre spécifié (ie. la valeur de l'arc qui lui est relié).
	 *
	 * @param nombre Le poids de l'arc
	 * @return true si le nombre de jetons dans la place est supérieur ou égal à nombre, sinon false.
	 */
	public boolean comparerJetons (int nombre) {
		if (this.getJetons()>= nombre) {
			return true;
		}
		return false;
	}
	
	
	

}
