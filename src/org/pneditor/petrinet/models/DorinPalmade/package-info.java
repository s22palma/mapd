/**
 * Ce paquetage contient les classes relatives à la création et 
 * à l'utilisation d'un réseau de Pétri. Il ne contient pas les 
 * méthodes d'une interface pour l'utilisateur.
 */
package org.pneditor.petrinet.models.DorinPalmade;