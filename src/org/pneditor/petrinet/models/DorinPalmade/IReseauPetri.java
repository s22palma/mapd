package org.pneditor.petrinet.models.DorinPalmade;

public interface IReseauPetri {
	
	void ajouterPlace(Place place);
	
	void supprimerPlace(int id);
	
	void ajouterArc(Arc arc);
	
	void supprimerArc(int id);
	
	void ajouterTransition(Transition tr);
	
	void supprimerTransition(int id);

	void AjouterArcsdetransition();

	void ajouterPlacedeArc();

	void supprimerArcdeT(int id);

	void tirer() throws NonTirable;
	
	public void afficher();

}
