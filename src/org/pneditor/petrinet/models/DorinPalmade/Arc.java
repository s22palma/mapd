package org.pneditor.petrinet.models.DorinPalmade;

/**
 * La classe 'Arc' représente un arc dans le réseau de Pétri. Il connecte une transition et une place.
 * Une instance de cette classe connait son identifiant, son poids (aussi appelé sa valeur) et la place à laquelle il est lié.
 */

public class Arc {

	private static int NB_ARC = 0;
	private final int id; // identifiant
	private int poids; //Le poid de l'arc
	private Place place; //La place à laquelle il est lié, que ce soit en sortie ou en entrée
	
	
	//Constructeur
	
	public Arc(int poids, Place place) {
		if (poids <0) {
			this.poids=0;
		}
		else {
			this.poids = poids;
		}
		this.place = place;
		NB_ARC++;
		this.id = NB_ARC;
	}


	//Getters
	
	public int getPoids() {
		return poids;
	}


	public void setPoids(int poids) {
		if (poids <0) {
			this.poids=0;
		}
		else {
			this.poids = poids;
		}
	}


	public Place getPlace() {
		return place;
	}


	public void setPlace(Place place) {
		this.place = place;
	}


	public int getId() {
		return id;
	}
	
	//Méthodes 
	
	public boolean estEntrant() {
		return false;
	}
	
	public boolean estSortant() {
		return false;
	}
	
	public boolean estZero() {
		return false;
	}
	
	public boolean estVideur() {
		return false;
	}
	
	
	
}
