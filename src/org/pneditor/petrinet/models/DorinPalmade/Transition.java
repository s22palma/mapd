package org.pneditor.petrinet.models.DorinPalmade;

import java.util.ArrayList;

public class Transition {
	private static int NB_TR=0; //permet de créer l'Id
	private final int id; //Rend chaque transition unique et identifiable
	private ArrayList<Sortant> sortants;
	private ArrayList<Entrant> entrants;
	
	
	public Transition() {
		this.entrants = new ArrayList<Entrant>();
		this.sortants = new ArrayList<Sortant>();
		NB_TR++;
		this.id = NB_TR;
	}
	
	public Transition(ArrayList<Sortant> sortants, ArrayList<Entrant> entrants) {
		this.entrants = entrants;
		this.sortants = sortants;
		NB_TR++;
		this.id = NB_TR;
	}
	
	public int getId() {
		return this.id;
	}
	
	public ArrayList<Entrant> getEntrant() {
		return this.entrants;
	}
	
	public ArrayList<Sortant> getSortant() {
		return this.sortants;
	}
	
	/**
	 * Cette méthode ajoute un arc entrant à la liste des arcs entrants, s'il n'existe pas déjà.
	 *
	 * @param entrant L'arc entrant à ajouter.
	 */
	public void ajouterEntrant(Entrant entrant) {
		boolean b = true;
		for(Arc a : this.entrants) b = b&(a.getId()!=entrant.getId());				
		if(b)this.entrants.add(entrant);
	}
	
	/**
	 * Cette méthode supprime l'arc entrant avec l'identifiant spécifié de la liste des arcs entrants.
	 *
	 * @param id L'identifiant de l'arc entrant à supprimer.
	 */
	public void supprimerEntrant(int id) {
		int i;
		for(i=0; i<this.entrants.size(); i++) {
			if(id==this.entrants.get(i).getId()) this.entrants.remove(i);
		}
	
	}
	
	/**
	 * Cette méthode ajoute un arc sortant à la liste des arcs sortants, s'il n'existe pas déjà.
	 *
	 * @param sortant L'arc sortant à ajouter.
	 */
	public void ajouterSortant(Sortant sortant) {
		boolean b = true;
		for(Arc a : this.sortants) b = b&(a.getId()!=sortant.getId());				
		if(b) this.sortants.add(sortant);
	}
	
	/**
	 * Cette méthode supprime l'arc sortant avec l'identifiant spécifié de la liste des arcs sortants.
	 *
	 * @param id L'identifiant de l'arc sortant à supprimer.
	 */
	public void supprimerSortant(int id) {
		int i;
		for(i=0; i<this.sortants.size(); i++) {
			if(id==this.sortants.get(i).getId()) this.sortants.remove(i);
		}
	
	}
	
	/**
	 * Pour chaque arc sortant, vérifie que la transition est tirable. Puis, pour chaque arc, effectue le tirage.
	 *
	 * @throws NonTirable Exception levée si la transition n'est pas tirable.
	 */
	public void incremente() throws NonTirable{
		boolean b = true;
		
		// Vérifie pour chaque arc sortant
		for(Sortant s : this.sortants) {
			int i = 0;
			ArrayList<Place> act = new ArrayList<Place>();
			
			// Si la place associée n'a pas encore été traitée
			if(!act.contains(s.getPlace())){
				act.add(s.getPlace());
				ArrayList<Sortant> lT = new ArrayList<Sortant>();
				
				// Compte le nombre d'arcs sortants associés à la même place
				for(Sortant si : this.sortants){
					if(s.getPlace()==si.getPlace()) {
						i++;
						lT.add(si);
					}
				}
				
				// Si plusieurs arcs sortants associés à la même place
				if(i>1){
					int v =0;
					int j =0;
					
					// Vérifie les conditions spécifiques pour chaque arc sortant
					for(Sortant si : lT) {
						if(si.getClass()==(new Zero(0, null).getClass())) {
							if(si.getPlace().getJetons()==0) {
								for(Sortant s1 : lT) b = b&(s1.getClass()==si.getClass());
							}
							else b = false;
						}
						else if (si.getClass()!= (new Videur(0, null).getClass()))
							j += si.getPoids();
						else v++;
					}
					
					// Vérifie si la transition est tirable en fonction des conditions
					if(v>0) b = b& (new Sortant(j+1, s.getPlace()).estTirable());
					else b = b& (new Sortant(j, s.getPlace()).estTirable());
				}
			}
		}
		// Vérifie si chaque arc sortant est tirable
		for(Sortant s:this.sortants) {
			b = b&s.estTirable();
		}
		// Si tous les arcs sont tirable, effectue le tirage
		if(b) {
			for(Sortant s:this.sortants) s.tirer();
			for(Entrant e:this.entrants) {
				e.tirer();
			}
			
		}
		else {
			throw new NonTirable();
		}
		
	}
	
	
	

	
	
	

	
}
