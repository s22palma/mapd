package org.pneditor.petrinet.models.DorinPalmade;

import java.util.ArrayList;

public class ReseauPetri implements IReseauPetri {
	private ArrayList<Place> places; //Liste des places du reseau de pétri
	private ArrayList<Transition> transitions; //Liste des transitions du reseau de pétri
	private ArrayList<Arc> arcs; //Liste des arcs du reseau de pétri
	
	
	//Constructeur d'une réseau de pétri vide
	public ReseauPetri() {
		this.places = new ArrayList<Place>();
		this.transitions = new ArrayList<Transition>();
		this.arcs = new ArrayList<Arc>();
	}
	
	//Getter
	
	public ArrayList<Place> getPlaces() {
		return places;
	}

	public ArrayList<Transition> getTransitions() {
		return transitions;
	}

	public ArrayList<Arc> getArcs() {
		return arcs;
	}

	
	
	
	//METHODE
	
	/**
	 * Cette méthode ajoute une place au réseau de Petri s'il n'y est pas déjà.
	 *
	 * @param place La place à ajouter.
	 */
	@Override
	public void ajouterPlace(Place place) {
		boolean b = true;
		for(Place p : this.places) {
			b = b&(p.getId()!=place.getId());
		}
		if(b) this.places.add(place);
		
	}
	
	/**
	 * Cette méthode ajoute toutes les places connectées aux arcs appartenant au réseau de Petri.
	 */
	@Override
	public void ajouterPlacedeArc() {
		for(Arc a : this.arcs) {
			boolean b = true;
			for(Place p : this.places) {
				b = b&(p.getId()!=a.getPlace().getId());
			}
			if(b) this.places.add(a.getPlace());
		}
	}
	
	/**
	 * Cette méthode supprime une place du réseau de Petri et tous les arcs qui y sont connectés.
	 *
	 * @param id L'identifiant de la place à supprimer.
	 */
	@Override
	public void supprimerPlace(int id) {
		int arc=0;
		for(int i=0; i<this.places.size(); i++) {
			if(id==this.places.get(i).getId()) this.places.remove(i);
		}
		for(int i=0; i<this.arcs.size(); i++) {
			if(id==this.arcs.get(i).getPlace().getId()) {
				arc=this.arcs.get(i).getId();
				this.arcs.remove(i);
			}
		}
		for(Transition t : this.transitions) {
			t.supprimerEntrant(arc);
			t.supprimerSortant(arc);
		}
	}
	
	
	
	
	/**
	 * Cette méthode ajoute un arc au réseau de Petri s'il n'y est pas déjà.
	 *
	 * @param arc L'arc à ajouter.
	 */
	@Override
	public void ajouterArc(Arc arc) {
		boolean b = true;
		for(Arc a : this.arcs) b = b&(a.getId()!=arc.getId());				
		if(b) this.arcs.add(arc);
	}
	
	/**
	 * Cette méthode ajoute tous les arcs connectés aux transitions appartenant au réseau de Petri.
	 */
	@Override
	public void AjouterArcsdetransition() {
		for(Transition t: this.transitions) {
			for(Entrant e : t.getEntrant()) {
				boolean b = true;
				for(Arc a : this.arcs) b = b&(a.getId()!=e.getId());				
				if(b) this.arcs.add(e);
			}
			for(Sortant s : t.getSortant()) {
				boolean b = true;
				for(Arc a : this.arcs) b = b&(a.getId()!=s.getId());				
				if(b) this.arcs.add(s);
			}
		}	
	}
	
	/**
	 * Cette méthode supprime un arc du réseau de Petri et également des listes des transitions.
	 *
	 * @param id L'identifiant de l'arc à supprimer.
	 */
	@Override
	public void supprimerArc(int id) {
		int i;
		for(i=0; i<this.arcs.size(); i++) {
			if(id==this.arcs.get(i).getId()) this.arcs.remove(i);
		}
		for(Transition t : this.transitions) {
			t.supprimerEntrant(id);
			t.supprimerSortant(id);
		}
		
	}
	

/**
 * Méthode appelée UNIQUEMENT dans supprimerTransition() pour supprimer du réseau de Petri
 * les arcs appartenant à la transition supprimée.
 *
 * @param id L'identifiant de l'arc à supprimer.
 */
	@Override
	public void supprimerArcdeT(int id) {
		int i;
		for(i=0; i<this.arcs.size(); i++) {
			if(id==this.arcs.get(i).getId()) this.arcs.remove(i);
		}
	}
	
	
	/**
	 * Cette méthode ajoute une transition au réseau de Petri s'il n'y est pas déjà.
	 *
	 * @param tr La transition à ajouter.
	 */
	@Override
	public void ajouterTransition(Transition tr) {
		boolean b = true;
		for(Transition t : this.transitions) b = b&(t.getId()!=tr.getId());				
		if(b) this.transitions.add(tr);
	}
	
	/**
	 * Cette méthode supprime une transition et tous les arcs qui y sont connectés du réseau de Petri.
	 *
	 * @param id L'identifiant de la transition à supprimer.
	 */
	@Override
	public void supprimerTransition(int id) {
		int i;
		for(i=0; i<this.transitions.size(); i++) {
			if(id==this.transitions.get(i).getId()) {
				for(Sortant s : this.transitions.get(i).getSortant()) {
					this.supprimerArcdeT(s.getId());
				}
				for(Entrant e : this.transitions.get(i).getEntrant()) {
					this.supprimerArcdeT(e.getId());
				}
				this.transitions.remove(i);
			}
		}
		
	}

	
	/**
	 * Pour toutes les transitions du réseau de Petri, appelle la fonction incremente.
	 *
	 * @throws NonTirable Exception levée si la transition n'est pas tirable.
	 */
	@Override
	public void tirer() throws NonTirable {
		for(Transition t: this.transitions) {
			t.incremente();
		}
	}
	
	
	/**
	 * Affiche des informations sur le réseau de Petri, y compris le nombre de places, transitions et arcs,
	 * ainsi que des détails sur chaque place, transition et arc.
	 */
	public void afficher() {
		int nbPlace = this.places.size();
		int nbT = this.transitions.size();
		int nbArc = this.arcs.size();
		
		System.out.println("Reseau de Petri");
		if(nbPlace>1) System.out.println(nbPlace + " places");
		else System.out.println(nbPlace + " place");
		if(nbT>1) System.out.println(nbT + " transitions");
		else System.out.println(nbT + " transition");
		if(nbArc>1) System.out.println(nbArc + " arcs");
		else System.out.println(nbArc + " arc");
		if(nbPlace>0) {
			System.out.println("Liste de places :");
			for(int i = 0; i<nbPlace; i++) {
				int e = 0;
				int s = 0;
				for(Arc a : this.arcs) {
					if(a.getPlace()==this.places.get(i)) {
						if(a.getClass()==(new Entrant(0, null).getClass())) e++;
						else s++;
					}
				}
				System.out.println( i+1 + " : place avec " + this.places.get(i).getJetons() + " jetons, " + s + " arc simple sortant, "+ e + " arc simple entrant");			
			}			
		}
		if(nbT>0) {
			System.out.println("Liste de transitions :");
			for(int i = 0; i<nbT; i++) {
				System.out.println( i+1 + " : transition, " + this.transitions.get(i).getEntrant().size() + 
						" arc entrant, " + this.transitions.get(i).getSortant().size() + " arc sortant");			
			}
		}
		
		if(nbArc>0) {
			System.out.println("Liste des arcs :");
			if(nbPlace>0) {
				for(int i = 0; i<nbArc; i++) {
					String s = "";
					if(this.arcs.get(i).getClass()==(new Sortant(0, null).getClass())) s = "Sortant";
					else if((this.arcs.get(i).getClass())==(new Videur(0, null).getClass())) s = "Videur";
					else if((this.arcs.get(i).getClass())==(new Zero(0, null).getClass())) s = "Zero";
					else s = "Entrant";
					int p = 0;
					for(int j = 0 ; j<nbPlace;j++) {
						if(this.arcs.get(i).getPlace().getId()==this.places.get(j).getId()) p=j+1;
					}
					if(s=="Videur"||s=="Zero") {
						System.out.println(i+1 + " : arc " + s + " (Connecté à la place " + p + 
						" avec " + this.arcs.get(i).getPlace().getJetons() + " jetons)");	
					}
					else System.out.println(i+1 + " : arc " + s + " de poids "
					+ this.arcs.get(i).getPoids() + " (Connecté à la place " + p + 
					" avec " + this.arcs.get(i).getPlace().getJetons() + " jetons)");	
				}
			}
			else {
				for(int i = 0; i<nbArc; i++) {
					String s = "";
					if(this.arcs.get(i).getClass()==(new Sortant(0, null).getClass())) s = "Sortant";
					else if((this.arcs.get(i).getClass())==(new Videur(0, null).getClass())) s = "Videur";
					else if((this.arcs.get(i).getClass())==(new Zero(0, null).getClass())) s = "Zero";
					else s = "Entrant";
					if(s=="Videur"||s=="Zero") System.out.println(i+1 + " : arc " + s);	
					else System.out.println(i+1 + " : arc " + s + " de poids "
							+ this.arcs.get(i).getPoids());	
				}
					
			}
			
		}
		
		
	}
	
	
	
}