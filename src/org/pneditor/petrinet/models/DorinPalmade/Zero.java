package org.pneditor.petrinet.models.DorinPalmade;

/**
 * La classe 'Zero' qui hérite de la classe 'Sortant' représente un arc zéro.
 * Un arc zéro est un arc dans le réseau de Pétri qui connecte une place à une transition.
 * Il ne peut être tirer que si la place à laquelle il est lié est vide.
 * 
 * L'attribut place de cette classe réfère donc à la classe en amont de l'arc sortant.
 */

public class Zero extends Sortant {

	
	//CONSTRUCTEURS
	public Zero(int poids, Place place) {
		super(poids, place);
		// TODO Auto-generated constructor stub
	}
	
	//METHODES
	
	/**
	 * Vérifie que l'arc peut être tiré, c'est à dire que la place en attribut est vide.
	 * 
	 * @return 'true' s'il peut être tiré, 'false' sinon.
	 */
	@Override
	public boolean estTirable() {
		return this.getPlace().getJetons()==0;
	}
	
	
	/**
	 * Tire l'arc zéro, c'est à dire ne fait rien
	 */
	@Override
	public void tirer() {
		
	}

	@Override
	public boolean estZero() {
		return true;
	}

	
}
