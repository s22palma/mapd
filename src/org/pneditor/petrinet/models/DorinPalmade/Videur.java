package org.pneditor.petrinet.models.DorinPalmade;

/**
 * La classe 'Videur' qui hérite de la classe 'Sortant' représente un arc videur.
 * Un arc videur est un arc dans le réseau de Pétri qui connecte une place à une transition.
 * Il vide la place à laquelle il est lié si elle n'est pas vide.
 * 
 * L'attribut place de cette classe réfère donc à la classe en amont de l'arc sortant.
 */

public class Videur extends Sortant {

	//CONSTRUCTEURS
	public Videur(int poids, Place place) {
		super(poids, place);
	}
	
	//METHODES
	
	/**
	 * Vérifie que l'arc peut être tiré, c'est à dire que la place en attribut est non vide.
	 * 
	 * @return 'true' s'il peut être tiré, 'false' sinon.
	 */
	@Override
	public boolean estTirable() {
		if (this.getPlace().getJetons()>0) {
			return true;
		}else return false;
	}
	/**
	 * Vide la place en attribut en mettant son nombre de jeton à 0
	 */
	//
	@Override
	public void tirer() {
		this.getPlace().supprimerJetons(this.getPlace().getJetons());
	}
	

	@Override
	public boolean estVideur() {
		return true;
	}
	

}
